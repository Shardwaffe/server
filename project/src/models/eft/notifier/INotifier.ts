export interface INotifierChannel
{
    server: string
    // eslint-disable-next-line @typescript-eslint/naming-convention
    channel_id: string
    url: string
    notifierServer: string
    ws: string
}
